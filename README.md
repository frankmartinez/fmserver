# Terraform Provider with FM Server

The FM server is an example of building a Terraform Provider using Mock API's. This project is meant to showcase building a Terraform Provider with CRUD operations and test cases.

## Table Of Contents

[TODO](TODO.md)

## Inputs

## Usage

## References

## Project Structure

```text

├── examples
│   ├── main.tf
│   ├── servers
│   └── terraform-provider-fmserver
├── fmserver
│   ├── api_client.go
│   ├── data_source_servers.go
│   └── provider.go
├── go.mod
├── go.sum
├── main.go
├── README.md
```

## Build Provider

Run the `go mod init` command to define the directory as the root of the module.

```sh
$ go mod init terraform-provider-fmserver
```

Then, run `go mod vendor` to create a `vendor` directory that contains all the provider's dependencies.

```sh
$ go mod vendor
```

Then, run the build command to build the binary. Terraform searches for plugins in the format `terraform-<TYPE>-<NAME>`. In this case the plugin is of type "provider" and the name is "fmserver".

```sh
$ go build -o terraform-provider-fmserver
```

## External Dependencies

Mock Services

- Return a server object: https://demo3650414.mockable.io/fmserver/00q

Example return block

```json
{
 "name": "demoserver001",
 "arch": "linux",
 "kernel":"2.5.6",
 "networking": {
     "ip_address": "192.168.2.1",
     "mac_address": "3432335434534"
 },
 "storage": {
   "type": "ssd",
   "capacity": "200GB"
 }
}
```

## Notes

1. Since Terraform data resources should only read information (not create, update or delete), only read (ReadContext) is defined. [Read More](https://learn.hashicorp.com/tutorials/terraform/provider-setup#define-coffees-data-resource)

These are the sources used to create this project.

- [Medium: Creating A Terraform Provider - Part 1](https://medium.com/spaceapetech/creating-a-terraform-provider-part-1-ed12884e06d7)

- [Hashicorp: Writing Custom Providers](https://www.terraform.io/docs/extend/writing-custom-providers.html)