# List of TODO Items

## Add CRUD operations

- [ ] Create a server
- [x] Read a server using a datasource
- [ ] Update a Server
- [ ] Delete a Server

## Update README Section

- [ ] Inputs
- [ ] Usage
- [ ] References

## Add Upgrade Section to support Terraform 13 providers

- [ ] Add UPGRADE.md
- [ ] Add updated readme section to show show to upgrade to Terraform 13
