module gitlab.com/frankmartinez/fmserver

go 1.14

require (
	github.com/hashicorp/terraform v0.12.28 // indirect
	github.com/hashicorp/terraform-plugin-sdk v1.15.0 // indirect
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.0.1
)
