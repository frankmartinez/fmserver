package fmserver

import (
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

// Provider Sameple
// Provider returns a terraform.ResourceProvider
func Provider() *schema.Provider {
	return &schema.Provider{
		DataSourcesMap: map[string]*schema.Resource{
			"fmservers": dataSourceServers(),
		},
	}
}

// func Provider() terraform.ResourceProvider {
// 	return &schema.Provider {
// 		ResourcesMap: map[string]*schema.Resource{
// 			// Define what to return
// 			// Returning the datasource
// 			DataSourcesMap: map[string]*schema.Resource{
// 				"fmsrevers": dataSourceServer(),
// 			},
// 		},
// 	}
// }
