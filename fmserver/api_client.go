package fmserver

import (
	"net/http"
	"time"
)

const (
	BaseURLV1 = "http://www.amock.io/api/fmartinez21/v1/server"
)

type Client struct {
	BaseURL		string
	HTTPClient	*http.Client
}

func NewClient() *Client {
	return &Client{
		BaseURL: BaseURLV1,
		HTTPClient: &http.Client{
			Timeout: time.Minute,
		},
	}
}

type errorResponse struct {
	Code 	int 	`json:"code"`
	Message string	`json:"message"` 
}

type successResponse struct {
	Code	int 		`json:"code"`
	Data	interface{}	`json:"data"`
}

